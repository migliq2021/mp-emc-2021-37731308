# EMC: 2021
# Nombre del Alumno: MIGUEL ANGEL LIQUITAYA
# DNI: 37731308

import os

#Función presionar tecla
def presionarTecla():
   input('Presione ENTER para continuar...')
   os.system('cls')

#Función menu con validación 
def menu():
   print('***Gestión de Empleados***')
   print('1- Agregar Empleados')
   print('2- Mostrar Empleados con id mayor o igual')
   print('3- Cambiar departamento a un empleado')
   print('4- Ordenar Empleados por nombre descendente')
   print('5- Salir')
   seleccion = int(input('Seleccione una opción: '))
   while not (seleccion >= 1) and (seleccion <= 5):
       seleccion = int(input('Seleccione una opción'))
   os.system('cls')   
   return seleccion

#Función leer dato convalidación de tamaño
def leerId():
    id = int(input('Ingrese id: '))
    return id

#Función validar con parametro de función anterior
def validarId(empleados, id):
   validado = True
   for dato in empleados:
       if id == dato[0]:
           validado = False
           break
   return validado

#Función leer dato convalidación de tamaño
def leerNombre():
    nombre = input('Ingrese Nombre: ')
    while not (nombre != ''):
        nombre = input('Ingrese Nombre: ')
    return nombre

#Función validar con parametro de función anterior
def validarEmail(empleados, email):
   validado = True
   for dato in empleados:
       if id == dato[0]:
           validado = False
           break
   return validado

#Función leer dato convalidación de tamaño
def leerEmail(empleados):
    emailNuevo = input('Ingrese Email: ')
    if validarEmail(empleados, emailNuevo):
        email = emailNuevo
    return email

#Función leer datos específicos y validarlo
def leerDepartamento():
    departamento = int (input('Seleccione el departamento 1=Gerencia, 2=Ventas, 3=Compras: '))
    while not ((departamento == 1) or (departamento == 2) or (departamento == 3)): 
        departamento = (input('Seleccione el departamento 1=Gerencia, 2=Ventas, 3=Compras: '))
    if departamento == 1:
        departamento = 'Gerencia'
    elif departamento == 2:
        departamento = 'Ventas'
    elif departamento == 3:
        departamento = 'Compras'
    return departamento

def leerSalario():
    salario = int(input('Ingrese Salario: '))
    while not (salario != ''):
        salario = input('Ingrese Salario: ')
    return salario

#Función agregar a lista con datos precargados
def agregarEmpleado(empleados):
    print('Cargar Empleados')
    empleados = [[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Compras", 3100]
            ]
    continuar = 's'
    while continuar == 's':
        id = leerId()
        if validarId (empleados, id):
            nombre = leerNombre()
            email = leerEmail(empleados)
            departamento = leerDepartamento()
            salario = leerSalario()
            empleados.append([id,nombre,email,departamento,salario])
            print('Empleado  Agregado...')
        else:
            print('Id ya existente...')
        continuar = input('¿Desea ingresar un empleado? s/n: ')
    return empleados

#Función mostrar lista
def mostrarEmpleados(empleados):
  x = int(input('Ingrese id: '))
  for datos in empleados:
      if datos[0] >= x:
         print(datos)

#Función buscar datos por dato llave con parametro de funcion anterior
def buscarPorId(empleados, id):
    pos = -1
    for i, datos in enumerate(empleados):
        if datos[0] == id:
            pos = i
            break
    return pos  

#Función cambiar depa
def cambiarDep(empleados):
   id = int(input('Ingrese el id del empleado que desea cambiar: '))
   pos = buscarPorId(empleados, id)
   if (pos != -1):
      depNuevo = int(input('Ingrese nuevo departamento 1=Gerencia, 2=Ventas, 3=Compras: '))
      if depNuevo == 1:
        empleados[pos][3] = 'Gerencia'
      elif depNuevo == 2:
        empleados[pos][3] = 'Ventas'
      elif depNuevo == 3:
        empleados[pos][3] = 'Compras'
      print ('Departamento cambiado...')
   else:
        print ('id inexistente')

def ordenarEmpleado(empleados):
    n = len(empleados)
    for i in range(n):
      for j in range(0, n-1):
        if empleados[j][1] < empleados[j+1][1]:
         empleados[j], empleados[j+1] = empleados[j+1], empleados[j]
    return empleados

#Menú Principal
os.system('cls')
opcion = 0
empleados = []
while  opcion != 5:
    opcion = menu()
    if opcion == 1:
      empleados = agregarEmpleado(empleados)
      presionarTecla()
    elif opcion == 2:
      mostrarEmpleados(empleados)
      presionarTecla()
    elif opcion == 3:
     cambiarDep(empleados)
     presionarTecla()
    elif opcion == 4:
     ordenarEmpleado(empleados) 
     print('Lista Ordenada por nombre')
     presionarTecla() 
    elif opcion == 5:
      input('Fin...')